<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:hostcms="http://www.hostcms.ru/"
	exclude-result-prefixes="hostcms">
	<xsl:output xmlns="http://www.w3.org/TR/xhtml1/strict" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" encoding="utf-8" indent="yes" method="html" omit-xml-declaration="no" version="1.0" media-type="text/xml"/>
	
	
	<xsl:template match="/">
		<xsl:apply-templates select="results"/>
			
	</xsl:template>
	
	<xsl:template match="results">
			
		<xsl:value-of disable-output-escaping="yes" select="log" />
		
			
		<xsl:if test="ajax = 0">
			<script>
				console.time('начали = ');
			</script>
			
		
		
			<div class="search-holder">
				
				<!-- Лого -->
				<a href="/"><div style="float: left; background: url(/templates/template22/images/logo2.png) no-repeat; width: 114px; height: 48px"></div></a>
				
				<xsl:choose>
				<xsl:when test="count(error) > 0">
				<!-- 	<div style="float: right; color: #cc0000; font-size: 18px; margin: 10px 30px; 10px;" class="error"><xsl:value-of disable-output-escaping="yes" select="error" /></div> -->
				</xsl:when>
				<xsl:otherwise>
					
					
					
					<span class="sometext" style="display: block; float:left; margin-left: 20px; width: 500px">
						<xsl:if test="query_date != '' and cache = 1">Показаны результаты запроса </xsl:if> <xsl:value-of select="query_date" /> <xsl:if test="query_date != '' and cache = 1">. <span id="recache_link_holder"></span></xsl:if>
					</span>
				
					<!-- Прогресс, статистика -->
					<div style="float: right; width: 510px" id="results-info">
						
						<span class="sometext" style="display: block; float: left; "><span id="search_proccess" style="color: red; font-weight: bold; text-decoration: blink;">Идет поиск ...</span> По запросу найдено <span id="count_results">0</span> результатов на <span id="count_stocks">0</span> из <span id="total_stocks_count"><xsl:value-of select="totalStocksCount"/></span> складах РФ</span>
						
						<span style="display: none;" id="count_results_in_spec_positions"><xsl:value-of select="count(//row)"/></span>
						
						
						<div class="bar" style="margin-top: 10px; float: right; margin-right: 10px;">
							<div class="progress"></div>
						</div>
						
						<xsl:if test="count(blackList/stock_id)">
							
							<div style="clear: right; margin-top: 5px;"><a href="#" class="clear-black-list">Очистить черный список складов (<xsl:value-of select="count(blackList/stock_id)" />)</a></div>
						
						</xsl:if>
					</div>
				</xsl:otherwise>	
				</xsl:choose>
				
				
				<!-- Форма поиска -->
				<form action="./" method="get" class="search-form" style="display: block; margin-top: 10px; float: left;  "> 
					
					<fieldset>
						<div class="holder">
							<div class="text" style="width: 200px;">
								<input type="text" name="nalichie" value="{search}" />
								<input name="number" type="hidden" value="0" />
							</div>
							<div class="btn-holder">
								<div class="btn-container">
									<div class="button size1">
										<span>искать</span>
										<input type="submit" value="1" />
									</div>
								</div>
								<!--<div class="btn-container">
									<a href="./?recache=1&amp;search={search}" class="button size1">
										<span>обновить запрос</span>
									</a>
								</div>-->
								
								<div class="btn-container">
									<a href="/sklady-mir/?nalichie={search}" class="button size1">
										<span>мировые склады</span>
									</a>
								</div>
								<div class="btn-container">
									<a href="#" class="button size1">
										<span>спецзапрос</span>
									</a>
									<div class="text-drop">
										<div class="holder">
											<span>От 1000 компонентов</span>
											<div class="link">
												<a href="#">Почему?</a>
											</div>
										</div>
									</div>
								</div>
							</div>						
						</div>
					</fieldset>
				</form>		
			
							
				
				<div style="clear: both" />
		
				<!-- <div id="step_holder"><xsl:value-of select="step" /></div>
				<div>sid_number=<xsl:value-of select="number" /></div> -->
						
			</div>
			<div style="clear: both" />
			
			<form action="./" method="get" class="check-form" id="check-form">
				<fieldset>
					<input type="hidden" name="checkFilter" value="1" />
					<input type="hidden" name="search" value="{/results/search}" />
					<xsl:call-template name="check-filter">
						<xsl:with-param name="checkListName" select="'image'"/>
					</xsl:call-template>
					<xsl:call-template name="check-filter">
						<xsl:with-param name="checkListName" select="'producers'"/>
					</xsl:call-template>
					<xsl:call-template name="check-filter">
						<xsl:with-param name="checkListName" select="'note'"/>
					</xsl:call-template>
					<xsl:call-template name="check-filter">
						<xsl:with-param name="checkListName" select="'p1'"/>
					</xsl:call-template>
					<xsl:call-template name="check-filter">
						<xsl:with-param name="checkListName" select="'p2'"/>
					</xsl:call-template>
					<xsl:call-template name="check-filter">
						<xsl:with-param name="checkListName" select="'p3'"/>
					</xsl:call-template>
					<xsl:call-template name="check-filter">
						<xsl:with-param name="checkListName" select="'instock'"/>
					</xsl:call-template>
				</fieldset>
			</form>	
			
		</xsl:if>
		
		<!-- РЕЗУЛЬТАТЫ СГРУППИРОВАННЫЕ ПО СКЛАДАМ -->
		
			<xsl:if test="ajax = 0">
				<div id="result_rows">
					
					<xsl:if test="//error">
						<div style="padding: 20px; border-bottom: 1px solid #999; text-align:center; font-weight: bold; font-size: 140%" class="error"><xsl:value-of disable-output-escaping="yes" select="error" /></div>
					</xsl:if>
										
					<xsl:if test="count(//error) = 0">
						<h1 style="text-align: center; font-size: 20px; display: none"><xsl:value-of select="search"/> - подбор поставщиков </h1>
					</xsl:if>
					
					<xsl:apply-templates select="stock_in_result" />
				</div>
				<div id="producers_block">
					<xsl:if test="ajax = 0 and count(stock_in_result) = 0">
						<xsl:attribute name="style">display: none</xsl:attribute>
					</xsl:if>
					<div class="news-heading">
						<h3>данные о поставщиках</h3>
					</div>
					<div class="table-holder last">
						<table class="table table2 color1" id="result_stocks_table">
							<thead>
								<tr>
									<th class="col1" style="width: 150px">Логотип</th>
									<th class="col2">Название склада</th>
									<th class="col3">Зарегистрирован</th>
									<th class="col4">Размещение</th>
									<th class="col5" style="width: 150px">Дистрибьютор</th>
								</tr>
							</thead>
							<tbody>
								<!-- -->
								<xsl:apply-templates select="all_stocks/stock_in_result" mode="bottom_table" />		
								<div id="result_stocks">
								</div>
							</tbody>
						</table>
					</div>
				</div>
				
				<!-- Скользящая панель, форма запроса -->
				<div class="bottom-panel">
					<div class="inner">
						<form action="/request/" method="post" id="request-form">
							<div class="request-box">
								<div class="holder">
									<span>Выбрано товаров:<strong id="count-request-parts">0</strong></span>
									<div class="reset">
										<a href="#">
											Сбросить выделение
											<input type="reset" id="request-reset" value="" />
										</a>
									</div>
								</div>
								<div class="button size1">
									<span>запросить</span>
									<input class="btn-request" type="submit" name="request" value=""/>
								</div>
							</div>
						</form>	
						<div class="filter-side" >
							
							<form action="./" method="get" id="select_filter_form" enctype="multypart/form-data">
								<input type="hidden" name="selectFilter" value="1" />
								<input type="hidden" name="nalichie" value="{/results/search}" />
								<div id="select_filter_button_holder">
									<a href="#" onclick="$(this).parent().parent().submit(); return false;" id="select_filter_button" class="button size1">
										<xsl:if test="count(/results/select_filter/sub_id) = 0 or /results/select_filter/sub_id = 'NULL' or /results/select_filter/sub_id = ''">
											<xsl:attribute name="onclick">alert_no_filter(this)</xsl:attribute>
										</xsl:if>
										<span>фильтровать</span>
									</a>
									<a href="#" id="reset_filter_link" >
										<xsl:if test="count(/results/select_filter/sub_id) = 0 or /results/select_filter/sub_id = 'NULL' or /results/select_filter/sub_id = ''">
											<xsl:attribute name="onclick">alert_no_filter(this)</xsl:attribute>
										</xsl:if>
										<span>сбросить фильтры</span>
									</a>													
								</div>
								<ul class="filter-items part-type" > 
									
									<li>
										<a href="#"><xsl:if test="/results/select_filter/sub_id = 'NULL'">
												<xsl:attribute name="onclick">alert_no_filter(this)</xsl:attribute>
											</xsl:if>
											Тип прибора</a>
										 <xsl:if test="/results/select_filter/sub_id = 'NULL'">
											: Не определен
										</xsl:if>
										<xsl:if test="count(/results/database/structures/structure)">
											<div class="select-drop">
												<div class="select-inner">
												
												<!--<xsl:value-of select="/results/database/structures/structure/*[name() = /results/database/sub_en]" />-->
												
												<select size="10" name="select_sub">
													<!--<xsl:apply-templates name="/results/database/values/*[name() = $property]" />-->
													<xsl:for-each select="/results/database/structures/structure">
														<xsl:variable name="sub_field" select="/results/database/sub_field" />
														
														<xsl:variable name="sub_name">
															<xsl:choose>
																<xsl:when test="*[name() = /results/database/sub_ru] != ''">
																	<xsl:value-of select="*[name() = /results/database/sub_ru]" />
																</xsl:when>
																<xsl:otherwise>
																	<xsl:value-of select="*[name() = /results/database/sub_en]" />
																</xsl:otherwise>
															</xsl:choose>
														</xsl:variable>
														
														<xsl:variable name="value" select="*[name()=$sub_field]" />
														<option value="{$value}">
															<xsl:if test="/results/select_filter/sub_id = $value">
																<xsl:attribute name="selected">true</xsl:attribute>
															</xsl:if>
															<xsl:value-of select="$sub_name" />													
														</option>
													</xsl:for-each>
												</select>
												</div>
											</div>
										</xsl:if>
										<xsl:if test="count(/results/database/structures/structure) = 0">
											: Не определен
										</xsl:if>
										<xsl:if test="/results/select_filter/sub_id > 0">
											<xsl:variable name="sub_id" select="/results/select_filter/sub_id" />
											<xsl:variable name="sub_field" select="/results/database/sub_field" />
											: <b class="selected-part-type"><xsl:value-of select="/results/database/structures/structure[*[name()=$sub_field] = $sub_id]/*[name() = /results/database/sub_ru]" />
											<xsl:text> </xsl:text>
											<xsl:value-of select="/results/database/structures/structure[*[name()=$sub_field] = $sub_id]/*[name() = /results/database/sub_en]" /> </b>										
											<xsl:if test="count(database/heads/*)">
											<!-- -->
											<li style="background: 0">	
												<a href="#" class="param-title" id="param_info_link">Параметрическая сводка</a>
												<div class="select-drop" style="width: 320px !important">
													<div class="select-inner" style="box-shadow: none">
														<ul>
														<xsl:apply-templates select="database/heads/*" mode="param_info" />
														</ul>
													</div>
												</div>
											</li>
											<xsl:if test="count(/results/database//structure/long_path)">


												<xsl:variable name="urlCatalog">
													<xsl:value-of select="/results/database//structure/long_path"/><xsl:if test="/results/database//structure/for_path"><xsl:value-of select="/results/database//structure/for_path"/>/</xsl:if>
												</xsl:variable>

												<li style="background: 0">	
													<a title="Подбор в каталоге ..." href="{$urlCatalog}?part={search}" id="catalog_link">Подбор в каталоге</a>
												</li>
											</xsl:if>
										</xsl:if>
										</xsl:if>
									</li>
								</ul>	
								<ul class="filter-items params">
									<xsl:apply-templates select="database/heads/*" />	
									
								</ul>
								
								<!--<xsl:if test="count(database/heads/*) and count(/results/select_filter/*[starts-with(name(), 'P')])">
									<a href="#" onclick="$(this).parent().reset(); return false;" class="button size1">
										<span>сбросить</span>
									</a>
								</xsl:if>-->
							</form>
						</div>
					</div>
				</div>
				
				
			</xsl:if>	
			<!-- только результаты ajax и склады -->
			<xsl:if test="count(//stock_in_result) > 0 and ajax = 1">
				<xsl:apply-templates select="stock_in_result" />
				<table id="result_stocks_offset_{step}" cellpadding="2" cellspacing="0" > <!-- style="display: none"> -->
					<xsl:apply-templates select="stock_in_result" mode="bottom_table" />
				</table>
			</xsl:if>
			
		<xsl:if test="END = 'false' and count(error) = 0">	
			
			<xsl:variable name="selectSub">select_sub=<xsl:value-of select="//select_filter/sub_id" /></xsl:variable>
			<xsl:variable name="selectFilter"><xsl:value-of select="//select_filter/select_filter_url" /></xsl:variable>
			<xsl:variable name="checkFilter">
				<xsl:for-each select="check_filter/*">check[<xsl:value-of select="name()" />][]=<xsl:value-of select="translate(check, ' ', '+')" /></xsl:for-each>
			</xsl:variable>
		
			<script>
				$(document).ready(function(){
					getQuery(
						<xsl:value-of select="count(//row) " />,
						<xsl:value-of select="count(stock_in_result)" />,
						<xsl:value-of select="ajax"/>,
						<xsl:value-of select="step"/>,
						'<xsl:value-of disable-output-escaping="yes" select="search"/>',
						'<xsl:value-of select="search_date_human"/>',
						<xsl:value-of select="number"/>, 
						'<xsl:value-of select="$selectSub"/>',			
						'<xsl:value-of disable-output-escaping="yes" select="$selectFilter"/>',			
						'<xsl:value-of select="$checkFilter"/>'			
					);				
				});
			</script>
		</xsl:if>
		<xsl:if test="END = 'true'">
		<script>
			HideLoadingScreen();
			$(function(){
				if ('<xsl:value-of select="search" />' != '') {
					$('#recache_link_holder').html('<a href="./?nalichie={search}&amp;recache=1">Обновить запрос</a>');
				}
				$('#search_proccess').html('Поиск завершен.');
				$('#search_proccess').attr('style', 'color: green');			
				$('.progress').animate({ 'left':  0 }, 500); 
			})
			//console.log('END of Search');
			console.timeEnd('закончили = ');
			
		</script>
		</xsl:if>
		
		
		
	</xsl:template>
	
	<xsl:template match="database/heads/*">
		<xsl:variable name="property" select="name()" />
		<xsl:if test="count(/results/database/values/*[name() = $property]) > 1">
		<li>
			<a class="param-title" href="#"><xsl:value-of select="."/></a>
			<div class="select-drop">
				<div class="select-inner">
				<select size="10" multiple="multiple" name="select[{$property}][]">
					<!--<xsl:apply-templates name="/results/database/values/*[name() = $property]" />-->
					<xsl:for-each select="/results/database/values/*[name() = $property]">
						<xsl:variable name="value" select="." />
						<option>
							<xsl:if test="/results/select_filter/*[name() = $property] = $value">
								<xsl:attribute name="selected">true</xsl:attribute>
							</xsl:if>
							<xsl:value-of select="." />
						</option>
					</xsl:for-each>
				</select>
				<div style="text-align: center"><a href="#" class="clear_select_filter_link">сбросить</a></div>
				</div>
			</div>				
		</li>
		</xsl:if>
	</xsl:template>
	<!-- Параметрическая сводка -->
	<xsl:template match="database/heads/*" mode="param_info">
		<xsl:variable name="property" select="name()" />
		<xsl:if test="count(/results/database/values/*[name() = $property]) = 1">
		<li>
			<xsl:value-of select="."/>: <xsl:text> </xsl:text>
			<xsl:for-each select="/results/database/values/*[name() = $property]">
				<b><xsl:value-of select="." /></b>
			</xsl:for-each>
		</li>
		</xsl:if>
	</xsl:template>
	
	
	
	<!-- Список складов в результатах внизу -->
	<xsl:template match="stock_in_result" mode="bottom_table">
		<xsl:variable name="accounttypeID" select="accounttype_id" />
		<xsl:variable name="AccountType" select="//chips_accounttype[@id = $accounttypeID]" />
		<xsl:variable name="companyID" select="company_id" />
		<xsl:variable name="Company" select="//chips_company[@id = $companyID]" />
		
		<tr>
			<td class="col1" style="width: 120px; height: 45px;">
				<!--<div class="visual-holder">
					<span class="visual">-->
						<xsl:variable name="src">
							<xsl:choose>
								<xsl:when test="$Company/image_small != ''"><xsl:value-of select="concat(//companyHref, $Company/image_small)" /></xsl:when>
								<xsl:otherwise>/templates/template22/img/img23.jpg</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<a href="#results{id}"><img src="{$src}" style="max-width: 120px; max-height: 45px;"  alt="image description"/></a>
					<!--</span>
				</div>-->
			</td>
			<td class="col2">
				<a href="#results{id}" class="name"><xsl:value-of disable-output-escaping="yes" select="name"/></a>
				<br/>
				<span style="font-size: 80%"><xsl:value-of select="location_name"/></span>
			</td>
			<td class="col3">
				<strong class="period new">Новый</strong>
			</td>
			<td class="col4">
				<strong class="{$AccountType/name}"><xsl:value-of disable-output-escaping="yes" select="$AccountType/name"/></strong>
			</td>
			<td class="col5">
				<xsl:if test="distrib = 2 or distrib = 1">
					<div class="status-ok"><img src="/templates/template22/img/icon9.png" width="17" height="15" alt="image description"/></div>
				</xsl:if>				
			</td>
		</tr>
	</xsl:template>
	
	<!-- Вывод результатов по складам -->
	<xsl:template match="stock_in_result">
		
		<xsl:variable name="stockID" select="id" />
		<xsl:variable name="companyID" select="company_id" />
		<xsl:variable name="Company" select="//chips_company[@id = $companyID]" />
				
		<xsl:variable name="color">
			
			<xsl:choose>
				<xsl:when test="spec_position = 3">2</xsl:when>
				<xsl:when test="spec_position > 0"><xsl:value-of select="spec_position" /></xsl:when>
				<xsl:otherwise>3</xsl:otherwise>
			</xsl:choose>
			
		</xsl:variable>
		
		<xsl:if test="position() = 1">
			
		</xsl:if>
		<div class="stock_block" rel="{$stockID}">
			<div class="form-wrap">
				<a name="results{$stockID}" />
					<div class="table-heading color{$color}">
						<div class="visual">
							<xsl:variable name="src">
								<xsl:choose>
									<xsl:when test="$Company/image_small != ''"><xsl:value-of select="concat(//companyHref, $Company/image_small)" /></xsl:when>
									<xsl:otherwise>/templates/template22/img/img23.jpg</xsl:otherwise>
								</xsl:choose>
							</xsl:variable>
							<!-- <a href="#" class="company-contacts-link"> -->
							<img src="{$src}" alt="{name}"/>
							<!-- </a> -->							
						</div>
						<h5>
							<!-- <a href="#" class="company-contacts-link"> -->
							<span style="color: #fff"><xsl:value-of select="name"/> - <xsl:value-of select="location_name"/></span>
							<!-- </a> --> 
							<!--<div class="debug" style="">
								spec=<xsl:value-of select="spec_position"/>
								weight=<xsl:value-of select="weight"/>
							</div>-->
							
							
						</h5>						
						<div class="contacts">
							<address><xsl:value-of disable-output-escaping="yes" select="$Company/contacts" /></address>
							<span class="phone">
								<xsl:choose>
									<xsl:when test="phone != ''"><xsl:value-of disable-output-escaping="yes" select="phone" /></xsl:when>
									<xsl:otherwise><xsl:value-of disable-output-escaping="yes" select="$Company/phone" /></xsl:otherwise>
								</xsl:choose>								
							</span>
							
							<xsl:choose>
								<xsl:when test="email != ''">
									<a href="mailto: {email}" class="mail">
										<xsl:value-of disable-output-escaping="yes" select="email" />
									</a>
								</xsl:when>
								<xsl:otherwise>
									<a href="mailto: {$Company/email}" class="mail">
										<xsl:value-of disable-output-escaping="yes" select="$Company/email" />
									</a>
								</xsl:otherwise>
							</xsl:choose>								
						
							<xsl:variable name="href">
								<xsl:choose>
									<xsl:when test="contains($Company/site, 'http://')"><xsl:value-of disable-output-escaping="yes" select="$Company/site" /></xsl:when>
									<xsl:otherwise>http://<xsl:value-of disable-output-escaping="yes" select="$Company/site" /></xsl:otherwise>
								</xsl:choose>
							</xsl:variable>
							<xsl:if test="$Company/site != ''">
								<a href="#" data-type="site" data-stock-id="{$stockID}" onclick="window.open('{$href}'); return false;" class="site stat">
									<xsl:value-of disable-output-escaping="yes" select="$href" />
								</a>
							</xsl:if>
						</div>
						
					</div>
					<div class="table-holder">
						<table class="table table1">
							<thead>
								<tr>
								<th class="col1"></th>
								<th class="col2">
									<div class="drop-link">
										<a href="#">Изобр.</a>
										<!--  -->
									</div>
								</th>
								<th class="col3"><div class="drop-link">
									<a href="#">Производитель</a>
																	
									</div></th>
								<th class="col4">
									<div class="drop-link">
										<a href="#">Наименование и комментарии</a>
										
									</div>
								</th>
								<th class="col5"><div class="drop-link"><a href="#">Розн.</a>
									
								</div></th>
								<th class="col6"><div class="drop-link"><a href="#">М. Опт.</a>
									
								</div></th>
								<th class="col7"><div class="drop-link"><a href="#">Опт.</a>
									
								</div></th>
								<th class="col8"><div class="drop-link"><a href="#">Склад</a>
									
									</div></th>
								</tr>
							</thead>
							<tbody>
								<xsl:apply-templates select="//row[stock_id = $stockID]"/>					
							</tbody>
						</table>
					</div>
					
				
			</div>
		
			<div class="buttons-line">
				<!--<xsl:if test="spec_position = 0">-->
					<a href="#" class="black-list">Добавить склад в черный список</a>
				<!--</xsl:if>-->
				<a href="./?reviews={$stockID}" class="reviews-link various fancybox.ajax">Отзывы</a>
				<span class="for"><xsl:value-of select="reviews"/></span>
				<!-- <span class="against">3</span> -->
				<a href="./?reviews={$stockID}&amp;add=1#add" class="leave-review various fancybox.ajax">Оставить отзыв</a>
			</div>
			
			<xsl:if test="spec_position > 0">
				<div class="spec_adds">
					<p>Это место рекламное. Хотите такое же? Закажите <a href="/docs/connect/alldocs/spec/">спецразмещение</a> вашего склада.</p>
				</div>
			</xsl:if>
		
		</div>		
		
		<!-- <div class="promo inbox margin1">
			<a href="#"><img src="/templates/template22/img/promo5.jpg" width="941" height="73" alt="image description"/></a>
		</div> -->
		
			
		
	</xsl:template> 
	
	<!-- Фильтр CHECKBOX в заголовках таблиц -->
	<xsl:template name="check-filter">
		<xsl:param name="checkListName" select="'yesno'" />
		<!-- <xsl:param name="stockID" select="'0'" /> -->
		<xsl:if test="count(/results/offset) = 1">
			<div class="check-drop">
				<div class="btn-holder">
					<input id="check[{$checkListName}][all]" name="check[{$checkListName}][all]" class="check check-all" type="checkbox"/>
					<label for="check[{$checkListName}][all]">Выбрать все</label>
					<a href="#" onclick="$('#check-form').submit(); e.preventDefault();" class="button size3">ok</a>
				</div>
				<div class="drop-cols">
					
						
						<xsl:variable name="realCheckList" >
							<xsl:choose>
							<xsl:when test="count(/results/checklist[name=$checkListName])"><xsl:value-of select="$checkListName" /></xsl:when>
							<xsl:otherwise><xsl:value-of select="'yesno'" /></xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						
						<xsl:for-each select="/results/checklist[name=$realCheckList]/li">
							<xsl:variable name="value" select="."/>
							<div class="frame">
								<input id="{$checkListName}_{position()}" name="check[{$checkListName}][]" value="{.}" class="check" type="checkbox">
									<xsl:if test="count(/results/check_filter/*[name() = $checkListName]/check[node() = $value]) > 0">
										<xsl:attribute name="checked">checked</xsl:attribute>
									</xsl:if>
								</input>
								<label for="{$checkListName}_{position()}"><xsl:value-of select="."/></label>
							</div>
						
						</xsl:for-each>
						
				</div>
			</div>
		</xsl:if>
	</xsl:template>
	
	
	<!-- Партномера в таблице -->
	<xsl:template match="row">
		
<tr>
<xsl:if test="position() mod 2 = 0">
<xsl:attribute name="class">odd</xsl:attribute>
</xsl:if>
<td class="c1 request-check"> 
<input name="{stock_id}_{part}"  type="checkbox"/>
<!-- <input name="{stock_id}_{part}" class="check" type="checkbox"/> -->
<!--<div class="checkboxArea"></div>-->
</td>
<td class="c2">
<span class="visual">
<xsl:variable name="src">
<xsl:choose>
<xsl:when test="img != ''"><xsl:value-of select="img" /></xsl:when>
<xsl:otherwise>/templates/template22/img/img4.jpg</xsl:otherwise>
</xsl:choose>
</xsl:variable>

<xsl:if test="isimg != 0">

<xsl:variable name="src_big">
<xsl:choose>
<xsl:when test="img_big != ''"><xsl:value-of select="img_big" /></xsl:when>
<xsl:otherwise></xsl:otherwise>
</xsl:choose>
</xsl:variable>

<a href="#" class="small_image"><img src="{$src}" width="46" height="42" alt="{part}"/></a>
<xsl:if test="img_big != ''">
<div class="big_image">
<img src="/images/ajax-loader-2.gif" rel="{$src_big}" alt="{part}"/> 
</div>
</xsl:if>
</xsl:if>
<xsl:if test="isimg = 0"> 
<img src="{$src}" width="46" height="42" alt="{part}"/>
</xsl:if>

</span>
<span class="b-t"></span>
<span class="b-b"></span>
</td>
<td class="c3">
<xsl:value-of disable-output-escaping="yes" select="mfr" />
<xsl:if test="distrib = 2">
<div class="image">
<div class="image-inner">
<img src="/templates/template22/img/icon6.png" width="13" height="13" alt="image description"/>
<div class="text-drop">
<div class="holder">
<span>Официальный дистрибьютор</span>
</div>
</div>
</div>
</div>
</xsl:if>

</td>
<!-- Название, описание, PDF  -->
<td class="c4">

<div class="icon">

<!-- <xsl:if test="more != ''">
	<a href="#" onclick="window.open('{more}'); return false;" target="_blank">Дополнительно</a>							
</xsl:if>
 -->
<xsl:choose>
<xsl:when test="pdf != ''">
<xsl:variable name="pdf"><xsl:value-of select="pdf" /></xsl:variable>
<a href="#" data-type="pdf" data-stock-id="{stock_id}" class="pdf stat" onclick="window.open('{$pdf}'); return false;" target="_blank"><img  src="/templates/template22/img/icon4.jpg" width="20" height="40" alt="описание PDF "/></a>							
</xsl:when>
<xsl:otherwise>
<img src="/templates/template22/img/icon4.jpg" width="20" height="40" alt="нет PDF"/>
</xsl:otherwise>
</xsl:choose>
</div>

<div class="heading">
	<div class="t-h">
		<a class="title more stat" href="#" onclick="return false;">
			<xsl:if test="more != ''">
				<xsl:attribute name="onclick">window.open('<xsl:value-of select="more" />'); return false;</xsl:attribute>
				<xsl:attribute name="data-type">more</xsl:attribute>
				<xsl:attribute name="data-stock-id"><xsl:value-of select="stock_id" /></xsl:attribute>
			</xsl:if>
			<xsl:if test="more = ''">
				<xsl:attribute name="class">no-active</xsl:attribute>
			</xsl:if>
			<xsl:value-of disable-output-escaping="yes" select="part" />
		</a>
		<span class="about"><xsl:value-of disable-output-escaping="yes" select="note" /></span>
	</div>
</div>

</td>
<xsl:variable name="currency">
	<xsl:choose>
		<xsl:when test="currency = 'RUR' or currency = 'RUB' or currency = 'rub' or currency = 'rur' or cur = 'RUR' or cur = 'RUB' or cur = 'rub' or cur = 'rur'"> руб. </xsl:when>
		<xsl:when test="currency = 'USD' or currency = 'usd' or currency = 'Dollar' or currency = 'dollar' or cur = 'USD' or cur = 'usd' or cur = 'Dollar' or cur = 'dollar'"> $</xsl:when>
		<xsl:when test="currency = 'EUR' or currency = 'euro' or currency = 'eur' or currency = 'EURO' or cur = 'EUR' or cur = 'euro' or cur = 'eur' or cur = 'EURO'"> € </xsl:when>
		<xsl:otherwise><xsl:value-of select="currency" /> </xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<td class="c5">


<xsl:if test="price1 != 0 and price1 != ''">
<xsl:value-of disable-output-escaping="yes" select="price1" /> <xsl:value-of disable-output-escaping="yes" select="$currency" />
</xsl:if>	
</td>
<td class="6">
<xsl:if test="price2 != 0 and price2 != ''">
<xsl:value-of disable-output-escaping="yes" select="price2" /> <xsl:value-of disable-output-escaping="yes" select="$currency" />
</xsl:if>	
</td>
<td class="c7">
<xsl:if test="price3 != 0 and price3 != ''">
<xsl:value-of disable-output-escaping="yes" select="price3" /> <xsl:value-of disable-output-escaping="yes" select="$currency" />
</xsl:if>
</td>
<td class="c8 last-child"> 
<xsl:if test="in_stock = 1">
<xsl:attribute name="style">font-weight: bold</xsl:attribute>
</xsl:if>
<xsl:value-of disable-output-escaping="yes" select="stock" />
</td>
</tr>
		
</xsl:template>
	
	
</xsl:stylesheet>