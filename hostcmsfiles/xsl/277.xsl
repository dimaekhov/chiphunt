<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:hostcms="http://www.hostcms.ru/"
	exclude-result-prefixes="hostcms">
	<xsl:output xmlns="http://www.w3.org/TR/xhtml1/strict" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" encoding="utf-8" indent="yes" method="html" omit-xml-declaration="no" version="1.0" media-type="text/xml" />

	<xsl:template match="/">
		<xsl:apply-templates select="/mouser" />
	</xsl:template>

	<xsl:variable name="category_id">
		<xsl:choose>
			<xsl:when test="count(/mouser/category_id) > 0 and /mouser/category_id != ''"> 
				<xsl:value-of select="/mouser/category_id" />
			</xsl:when>
			<xsl:otherwise>0</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="sub1_id">
		<xsl:choose>
			<xsl:when test="count(/mouser/sub1_id) > 0 and /mouser/sub1_id != 0">
				<xsl:value-of select="/mouser/sub1_id" />
			</xsl:when>
			<xsl:otherwise>0</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="sub2_id">
		<xsl:choose>
			<xsl:when test="count(/mouser/sub2_id) > 0 and /mouser/sub2_id != 0">
				<xsl:value-of select="/mouser/sub2_id" />
			</xsl:when>
			<xsl:otherwise>0</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="sub3_id">
		<xsl:choose>
			<xsl:when test="count(/mouser/sub3_id) > 0 and /mouser/sub3_id != 0">
				<xsl:value-of select="/mouser/sub3_id" />
			</xsl:when>
			<xsl:otherwise>0</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	
	<xsl:variable name="catalog_url" select="/mouser/catalog_url" />
	
	<!-- Определяем группу для формирования адреса ссылки -->
	<xsl:variable name="group_link">
		<xsl:value-of select="$catalog_url" />
			<xsl:if test="$category_id > 0">
				<xsl:value-of select="//category/for_path" />/
				<xsl:if test="$sub1_id > 0">
					<xsl:value-of select="//sub1_structure/for_path" />/
					<xsl:if test="$sub2_id > 0">
						<xsl:value-of select="//sub2_structure/for_path" />/
						<xsl:if test="$sub3_id > 0">
							<xsl:value-of select="//sub3_structure/for_path" />/
						</xsl:if>
					</xsl:if>
				</xsl:if>
			</xsl:if>	
	</xsl:variable>
	
	
	<xsl:template match="/mouser">
		
		<a name="catalog"></a>
	
		<div class="main-heading">
			<div class="bredcrumbs-holder">
				<ul class="bredcrumbs">
					<li><a href="/">главная страница</a></li>
					<li><a href="{$catalog_url}">каталог</a></li>
					<xsl:if test="$sub1_id > 0">
						<xsl:if test="$category_id > 0">
							<li><a href="{$catalog_url}{//category/for_path}/"><xsl:value-of select="//category/cat_ru_name" /></a></li>
							<xsl:if test="$sub2_id > 0">
								<li><a href="{$catalog_url}{//category/for_path}/{//sub1_structure/for_path}/"><xsl:value-of select="//sub1_structure[sub1_id = $sub1_id]/sub1_ru_name" /></a></li>
									<xsl:if test="$sub3_id > 0">
										<li><a href="{$catalog_url}{//category/for_path}/{//sub1_structure/for_path}/{//sub2_structure/for_path}/"><xsl:value-of select="//sub2_structure[sub2_id = $sub2_id]/sub2_ru_name" /></a></li>
									</xsl:if>
							</xsl:if>	
						</xsl:if>											
					</xsl:if>
				</ul>
				<h1>
					<!-- <a href="{$group_link}"> -->
					<xsl:choose>
						
						<xsl:when test="count(/mouser/seo_h1) and /mouser/seo_h1 != ''">	
							<xsl:value-of select="/mouser/seo_h1" disable-output-escaping="yes" />
						</xsl:when>
						
						<xsl:when test="$sub3_id > 0">	
							<xsl:value-of select="structures//sub3_structure[sub3_id = $sub3_id]/sub3_ru_name" />
						</xsl:when>
						<xsl:when test="$sub2_id > 0">	
							<xsl:value-of select="structures//sub2_structure[sub2_id = $sub2_id]/sub2_ru_name" />
						</xsl:when>
						<xsl:when test="$sub1_id > 0">	
							<xsl:value-of select="structures//sub1_structure[sub1_id = $sub1_id]/sub1_ru_name" />
						</xsl:when>
						<xsl:otherwise>	
							<xsl:value-of select="structures/category[cat_id = $category_id]/cat_ru_name" />
						</xsl:otherwise>
					</xsl:choose>
					<!-- </a> -->
				</h1>
			</div>
			<span class="catalog-lang">Каталог на русском <a href="#">english</a></span>
		</div>
			
			<xsl:if test="count(error404)">
				<h2>Ошибка 404. Страницы не существует.</h2>			
			</xsl:if>
		
			<xsl:choose>
				
				<xsl:when test="$category_id  > 0 and $sub1_id > 0 and count(partnumbers)">
				<!--<xsl:when test="total &gt; 0 and limit &gt; 0">-->
					
				<!--data-->
					
					<xsl:variable name="count_pages" select="ceiling(total div limit)"/>
					
					<xsl:variable name="visible_pages" select="10"/> 
					
					<xsl:variable name="real_visible_pages"><xsl:choose>
							<xsl:when test="$count_pages &lt; $visible_pages"><xsl:value-of select="$count_pages"/></xsl:when>
							<xsl:otherwise><xsl:value-of select="$visible_pages"/></xsl:otherwise>
					</xsl:choose></xsl:variable>
					
					<!-- Считаем количество выводимых ссылок перед текущим элементом -->
					<xsl:variable name="pre_count_page"><xsl:choose>
							<xsl:when test="page - (floor($real_visible_pages div 2)) &lt; 0">
								<xsl:value-of select="page"/>
							</xsl:when>
							<xsl:when test="($count_pages - page - 1) &lt; floor($real_visible_pages div 2)">
								<xsl:value-of select="$real_visible_pages - ($count_pages - page - 1) - 1"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:choose>
									<xsl:when test="round($real_visible_pages div 2) = $real_visible_pages div 2">
										<xsl:value-of select="floor($real_visible_pages div 2) - 1"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="floor($real_visible_pages div 2)"/>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:otherwise>
					</xsl:choose></xsl:variable>
					
					<!-- Считаем количество выводимых ссылок после текущего элемента -->
					<xsl:variable name="post_count_page"><xsl:choose>
							<xsl:when test="0 &gt; page - (floor($real_visible_pages div 2) - 1)">
								<xsl:value-of select="$real_visible_pages - page - 1"/>
							</xsl:when>
							<xsl:when test="($count_pages - page - 1) &lt; floor($real_visible_pages div 2)">
								<xsl:value-of select="$real_visible_pages - $pre_count_page - 1"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$real_visible_pages - $pre_count_page - 1"/>
							</xsl:otherwise>
					</xsl:choose></xsl:variable>
					
					<xsl:variable name="i"><xsl:choose>
							<xsl:when test="page + 1 = $count_pages"><xsl:value-of select="page - $real_visible_pages + 1"/></xsl:when>
							<xsl:when test="page - $pre_count_page &gt; 0"><xsl:value-of select="page - $pre_count_page"/></xsl:when>
							<xsl:otherwise>0</xsl:otherwise>
					</xsl:choose></xsl:variable>
					
					<div class="results-block">
						<span class="border-line"></span>
						<span class="border-line2"></span>
						<div class="results-line">
							<span class="all-results">Всего результатов: <span><xsl:value-of select="total" /></span></span>
							<strong class="filter-results">Если применить фильтр: <div class="ajax-loader"></div><span><xsl:value-of select="total" /></span></strong>
						</div>
						<form action="{$group_link}" class="select-form" id="select-form-filter" method="get" enctype="multipart/form-data"> 
								<div class="scroll-pane for-select">
									<ul class="select-list">
									<xsl:if test="count(/mouser/partnumbers/partnumber) > 1">
									<li>
											<em class="title">Компонент</em>
											<div class="select-holder">
												<div class="select-frame" style="position: relative"> 
													<select style="width:188px;" id="partnumber" name="partnumber[]" size="10" multiple="multiple" >
														<xsl:for-each select="/mouser/partnumbers/partnumber">
															<option>
																<xsl:if test=" . = /mouser/filter/partnumber">
																	<xsl:attribute name="selected">1</xsl:attribute>
																</xsl:if>
																<xsl:value-of select="." />
															</option>
														</xsl:for-each>
													</select>
													<div class="ajax-loader-2"></div>
												</div>
											</div>
											<div>
												<xsl:attribute name="class">reset-item 
													<xsl:if test="count(/mouser/filter/partnumber) = 0">
														disable
													</xsl:if> 
												</xsl:attribute>
												<a href="#" class="reset-frame">
													Сбросить
												</a>
												<span class="hover"></span>
											</div>
										</li>
									</xsl:if>	
										<li>
											<em class="title">Производитель</em>
											<div class="select-holder">
												<div class="select-frame"> 
													<select style="width:188px;" name="producer[]" size="10" multiple="multiple" >
														<xsl:for-each select="/mouser/producers/producer">
															<option>
																<xsl:if test=" . = /mouser/filter/producer">
																	<xsl:attribute name="selected">1</xsl:attribute>
																</xsl:if>
																<xsl:value-of select="." />
															</option>
														</xsl:for-each>
													</select>
												</div>
											</div>
											<div>
												<xsl:attribute name="class">reset-item 
													<xsl:if test="count(/mouser/filter/producer) = 0">
														disable
													</xsl:if> 
												</xsl:attribute>
												<a href="#" class="reset-frame">
													Сбросить
												</a>
												<span class="hover"></span>
											</div>
										</li>
										
										<!-- Фильтры по параметрам -->
										<xsl:apply-templates select="/mouser/head_mouser/*" mode="select-filter" />
									</ul>
								</div>
								<div class="filter-nav" style="width: 100% !important"> 
									<div class="reset">
										<span>
											<input type="reset" value="" />
											Сбросить все фильтры
										</span>
									</div>
									<div class="button submit">
										<span>применить фильтры</span>
										<input type="submit" name="select-filter-submit" value="1" />
									</div>   
									<div class="part-like">
										<label>Название компонента:</label>
										<input type="text" name="part" id="part" class="text"  value="{/mouser/filter/part}"/>
									</div>
									<div class="clear: both"/>									
								</div>
						</form>
					</div>
					<!-- пагинатор -->
					<ul class="paging change">
						<xsl:call-template name="for">
							<xsl:with-param name="limit" select="limit"/>
							<xsl:with-param name="page" select="page"/>
							<xsl:with-param name="items_count" select="total"/>
							<xsl:with-param name="i" select="$i"/>
							<xsl:with-param name="post_count_page" select="$post_count_page"/>
							<xsl:with-param name="pre_count_page" select="$pre_count_page"/>
							<xsl:with-param name="visible_pages" select="$real_visible_pages"/>
						</xsl:call-template>
					</ul>
					
					<xsl:variable name="filter_url">
						<xsl:choose>
							<xsl:when test="/mouser/filter/url != ''">
								<xsl:value-of select="/mouser/filter/url"/>&amp;
							</xsl:when>    
							<xsl:otherwise>?</xsl:otherwise>
						</xsl:choose>						
					</xsl:variable>
					
					<div class="new-fix" style="">
							<div class="fix-panel">
								<table class="table table4">
									<thead>
										<tr>
											<th>
												<xsl:if test="/mouser/order = 'img_local'">
													<xsl:attribute name="class">active</xsl:attribute>
												</xsl:if>
												<a href="{$group_link}{$filter_url}order=img_local" class="item">Изобр.</a></th>
											<th>
												<xsl:if test="/mouser/order = 'part'">
													<xsl:attribute name="class">active</xsl:attribute>
												</xsl:if>
												<a href="{$group_link}{$filter_url}order=part" class="item">Партномер</a></th>
											<th>
												<xsl:if test="/mouser/order = 'mfr'">
													<xsl:attribute name="class">active</xsl:attribute>
												</xsl:if>
												<a href="{$group_link}{$filter_url}order=mfr" class="item">Производитель</a>
											</th>
											<th>
												<xsl:attribute name="class">description
													<xsl:if test="/mouser/order = 'note'">
														active
													</xsl:if>
												</xsl:attribute>
												<a href="{$group_link}{$filter_url}order=note" class="item">Описание</a></th>
											<th>
												<xsl:if test="/mouser/order = 'pdf_local'">
													<xsl:attribute name="class">active</xsl:attribute>
												</xsl:if>
												<a href="{$group_link}{$filter_url}order=pdf_local" class="item">Data-sheet</a></th>

											<xsl:for-each select="head_mouser/*"> 
												<th>
													<xsl:if test="/mouser/order = name()">
														<xsl:attribute name="class">active</xsl:attribute>
													</xsl:if>
													<a title="При сортировке по данному параметру, компоненты с пустым значением параметра исключаются из выборки!" href="{$group_link}{$filter_url}order={name()}" class="item"><xsl:value-of select="." /></a>
												</th>
											</xsl:for-each>
										</tr>
									</thead>
								</table>
							</div>
							<div class="void-block"></div>
							</div>
					
					<div class="scroll-container">
						<span class="border-line3"></span>
						<div class="scroll-pane for-table">
							
							<div class="table-holder">
								<table class="table table4 pos1">
									<thead>
										<tr>
											<th>
												<xsl:if test="/mouser/order = 'img_local'">
													<xsl:attribute name="class">active</xsl:attribute>
												</xsl:if>
												<a href="{$group_link}{$filter_url}order=img_local" class="item">Изобр.</a></th>
											<th>
												<xsl:if test="/mouser/order = 'part'">
													<xsl:attribute name="class">active</xsl:attribute>
												</xsl:if>
												<a href="{$group_link}{$filter_url}order=part" class="item">Партномер</a></th>
											<th>
												<xsl:if test="/mouser/order = 'mfr'">
													<xsl:attribute name="class">active</xsl:attribute>
												</xsl:if>
												<a href="{$group_link}{$filter_url}order=mfr" class="item">Производитель</a>
											</th>
											<th>
												<xsl:attribute name="class">description
													<xsl:if test="/mouser/order = 'note'">
														active
													</xsl:if>
												</xsl:attribute>
												<a href="{$group_link}{$filter_url}order=note" class="item">Описание</a></th>
											<th>
												<xsl:if test="/mouser/order = 'pdf_local'">
													<xsl:attribute name="class">active</xsl:attribute>
												</xsl:if>
												<a href="{$group_link}{$filter_url}order=pdf_local" class="item">Data-sheet</a></th>
											<xsl:for-each select="head_mouser/*"> 
												<th>
													<xsl:if test="/mouser/order = name()">
														<xsl:attribute name="class">active</xsl:attribute>
													</xsl:if>
													<a title="При сортировке по данному параметру, компоненты с пустым значением параметра исключаются из выборки!" href="{$group_link}{$filter_url}order={name()}" class="item"><xsl:value-of select="." /></a>
												</th>
											</xsl:for-each>
										</tr>
									</thead>
									<tbody>
										
										<!-- Данные Партномера в таблице -->
										<xsl:apply-templates select="data_mouser/data" />
										
										
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<!-- Постраничная навигация -->
					<ul class="paging change">
						<xsl:call-template name="for">
							<xsl:with-param name="limit" select="limit"/>
							<xsl:with-param name="page" select="page"/>
							<xsl:with-param name="items_count" select="total"/>
							<xsl:with-param name="i" select="$i"/>
							<xsl:with-param name="post_count_page" select="$post_count_page"/>
							<xsl:with-param name="pre_count_page" select="$pre_count_page"/>
							<xsl:with-param name="visible_pages" select="$real_visible_pages"/>
						</xsl:call-template>
					</ul> 
				
					<script>
						$(function(){
							
							
						});
					</script>
					
				</xsl:when>
				
				<xsl:when test="$category_id > 0">
					
					<!--subs-->
					<script>
						$(function(){
							
							
							//$('.accordion .title a').click(function(e){
							//	e.preventDefault();
							//});
							
							$('.accordion .title').click(function(){
								if($(this).closest('li').hasClass('inbox')){
									$(this).closest('li').find('.expanded').slideToggle(500);
									$(this).prev().toggleClass('active');
									$(this).closest('li').toggleClass('active');
									
									return false;
								}
							})
							
							$('.accordion .plus-minus').click(function(){
								$(this).parent().next('.expanded').slideToggle(500);
								$(this).toggleClass('active');
								return false;
							})
						
						})
					</script>
					<div class="accordion">
						<ul class="accordion-list">
							<xsl:apply-templates select="structures/category[cat_id = $category_id]/sub1_structures/sub1_structure" />
						</ul>
						<div style="clear: both"/>
					</div>
				</xsl:when>
				
				<xsl:otherwise>
					<!--categories-->
					
					<ul class="components-list">
						<xsl:apply-templates select="structures/category" />
					</ul>
					
				</xsl:otherwise>
			</xsl:choose>
			
			
		<div class="clear: both" />	
		
	</xsl:template>
	  
	<xsl:template match="data_mouser/data">
		
		<xsl:variable name="data" select="." />
		
		<tr>
			<xsl:if test="position() mod 2 = 0">
				<xsl:attribute name="class">odd</xsl:attribute>
			</xsl:if>
			<td style="padding: 3px;">
				<div class="visual-holder">
					<span class="visual">

						<a data-title="{part}" href="#" onclick="return false;"><img src="http://img.chiphunt.ru/img.php?decode={img_local}&amp;part={part}" width="46" height="42" alt="{part}" /></a>
						<xsl:if test="img_local  != 0 ">
							<span class="hidden">
								<img src="http://img.chiphunt.ru/img_sml.php?decode=0" rel="http://img.chiphunt.ru/img_sml.php?decode={img_local}&amp;part={part}" alt="{part}" />
							</span>
						</xsl:if>

					</span>
				</div>
			</td>
			<td class="active">
				<div class="part-number">
					<div class="drop-holder">
						<a href="#" onclick="return false;"><xsl:value-of select="part" /></a>
						<div class="text-drop size2" style="width: 280px !important; padding: 3px;">
							<div class="holder">								
								<div class="link">
									<a href="/sklady-rf/?nalichie={part}">Запросить на складах РФ</a>
								</div>
								<div class="link">
									<a href="/sklady-mir/?nalichie={part}&amp;number=0&amp;find=Поиск">Запросить на мировых складах</a>
								</div>
							</div>
						</div>
					</div>
					<!--<span>Устарело</span>-->
				</div>
			</td>
			<td>
				<xsl:value-of select="mfr" />
			</td>
			<td class="left description">
				<xsl:value-of disable-output-escaping="yes" select="note" />
			</td>
			<td>
				<div class="icon">
					<xsl:choose>
						<xsl:when test="pdf_local > 0">
							<!--<a href="#" onclick="window.open('http://pdf1.chiphunt.ru/pdf.php?decode={pdf_local}&amp;part={part}'); return false;" target="_blank"><img src="/templates/template22/img/icon4.jpg" width="20" height="40" alt="{part}"/></a>-->
                            <a href="http://pdf1.chiphunt.ru/pdf.php?decode={pdf_local}&amp;part={part}" target="_blank"><img src="/templates/template22/img/icon4.jpg" width="20" height="40" alt="{part}"/></a>
						</xsl:when>
						<xsl:otherwise>
						<img src="/templates/template22/img/icon4.jpg" width="20" height="40" alt="нет"/>
						</xsl:otherwise>
					</xsl:choose>
				</div>				
			</td>
			
			<!-- параметры -->
			<xsl:for-each select="/mouser/head_mouser/*">
				<xsl:variable name="property" select="name()" />
				<td>
					<xsl:value-of select="$data/*[name() = $property]" />
				</td>
			</xsl:for-each>
		</tr>
		
	</xsl:template>
	
	<xsl:template match="/mouser/head_mouser/*" mode="select-filter">
		<xsl:variable name="property" select="name()" />
		<xsl:if test="count(/mouser/values/*[name() = $property])">
		<li>
			<em class="title"><xsl:value-of select="." /></em>
			<div class="select-holder">
				<div class="select-frame">   
					<select size="10" multiple="multiple" name="param[{$property}][]">
						<xsl:for-each select="/mouser/values/*[name() = $property]">
							<xsl:variable name="value" select="." />
							<option>
								<xsl:if test="/mouser/filter/*[name() = $property] = $value">
									<xsl:attribute name="selected">1</xsl:attribute>
								</xsl:if>
								<xsl:value-of select="." />
							</option>
						</xsl:for-each>
					</select>
				</div>
			</div>
			<div>
				<xsl:attribute name="class">reset-item 
					<xsl:if test="count(/mouser/filter/*[name() = $property]) = 0">
						disable
					</xsl:if>
				</xsl:attribute>
				<a href="#" class="reset-frame">
					Сбросить
				</a>
				<span class="hover"></span>
			</div>
		</li>
		</xsl:if>
	</xsl:template>
	
	
	<xsl:template match="sub1_structure">
		<li>
			<xsl:if test="count(sub2_structures/sub2_structure)">
				<xsl:attribute name="class">inbox</xsl:attribute>
			</xsl:if>
			
			<xsl:variable name="active"><xsl:if test="$sub1_id = @id or sub2_structures/sub2_structure/@id = $sub2_id"> active</xsl:if></xsl:variable>
			
			<div class="heading">
				<xsl:if test="count(sub2_structures/sub2_structure)">
					<div class="plus-minus{$active}"></div>
				</xsl:if>
				<strong class="title"><a href="{$catalog_url}{../../for_path}/{for_path}/"><xsl:value-of select="sub1_ru_name" /></a></strong>
				<span><xsl:value-of select="count_subs"/></span>
			</div>
			<xsl:if test="count(sub2_structures/sub2_structure)">
				<div class="expanded">
					<xsl:if test="count(//sub1_structure) = 1">
						<xsl:attribute name="style">display: block</xsl:attribute>
					</xsl:if>
					<ul class="list">
						<xsl:apply-templates select="sub2_structures/sub2_structure" />
					</ul>
				</div>
			</xsl:if>
		</li>
		
	</xsl:template>
	<xsl:template match="sub2_structure">
		<li>
			<xsl:choose>
			<xsl:when test="count(sub3_structures/sub3_structure)">
				<xsl:attribute name="class">inbox</xsl:attribute>
			
				<xsl:variable name="active"><xsl:if test="$sub1_id = @id or sub2_structures/sub2_structure/@id = $sub2_id or sub3_structures/sub3_structure/@id = $sub3_id"> active</xsl:if></xsl:variable>
				
				<div class="heading">
					<div class="plus-minus{$active}"></div>
					<strong class="title"><a href="{$catalog_url}{../../../../for_path}/{../../for_path}/{for_path}/"><xsl:value-of select="sub2_ru_name" disable-output-escaping="yes"  /></a></strong>
					<span><xsl:value-of select="count_subs"/></span>
				</div>
				<div class="expanded">
					<xsl:if test="count(//sub2_structure) = 1">
						<xsl:attribute name="style">display: block</xsl:attribute>
					</xsl:if>
					<ul class="list">
						<xsl:apply-templates select="sub3_structures/sub3_structure" />
					</ul>
				</div>
			</xsl:when>
			<xsl:otherwise>
				<a href="{$catalog_url}{../../../../for_path}/{../../for_path}/{for_path}/"><xsl:value-of select="sub2_ru_name" disable-output-escaping="yes" /></a>
			</xsl:otherwise>	
			</xsl:choose>
		</li>
		
	</xsl:template>
	<!-- <xsl:template match="sub2_structure">
		<li>
			<a href="{$catalog_url}{../../../../for_path}/{../../for_path}/{for_path}/"><xsl:value-of select="sub2_ru_name" /></a>
		</li>		
	</xsl:template> -->
	<xsl:template match="sub3_structure"> 
		<li>
			<a href="{$catalog_url}{../../../../../../for_path}/{../../../../for_path}/{../../for_path}/{for_path}/"><xsl:value-of select="sub3_ru_name" /></a>
		</li>		
	</xsl:template>
	
	<xsl:template match="category">
		<xsl:variable name="cat_id" select="cat_id" />
		<li>
			<div class="box">
				<div class="box-frame">
					<xsl:variable name="img_src">
						<xsl:choose>
							<xsl:when test="count(/mouser/chips_setting[name = concat('cat_', $cat_id, '_img')])"> 
								<xsl:value-of select="/mouser/chips_setting[name = concat('cat_', $cat_id, '_img')]/value"/>
							</xsl:when>
							<xsl:otherwise>/templates/template16/images/img8.png</xsl:otherwise>							
						</xsl:choose>						
					</xsl:variable>
					<div class="visual"><img src="{$img_src}" width="57" height="51" alt="image description" /></div>
					<div class="description">
						<strong class="title"><a href="{$catalog_url}{for_path}/"><xsl:value-of select="cat_ru_name" disable-output-escaping="yes" /></a></strong>
						
						<p><xsl:call-template name="category-description">
							<xsl:with-param name="category" select="$cat_id"/>
							<xsl:with-param name="position" select="1"/>
							<xsl:with-param name="length" select="0"/>							
						</xsl:call-template>
						</p>
						
					</div>
				</div> 
			</div>
		</li>		
	</xsl:template>
	
	<!-- Рекурсивный вывод подразделов в описании категории каталога -->
	<xsl:template name="category-description">
		<xsl:param name="category" />
		<xsl:param name="position" />
		<xsl:param name="length" />							
		
		<xsl:if test="count(//category[cat_id = $category]//sub1_structure[position() = $position])">
			<xsl:apply-templates select="//category[cat_id = $category]//sub1_structure[position() = $position]"  mode="category-description" />
			
			<xsl:variable name="new_length" select="$length + string-length(//category[cat_id = $category]//sub1_structure[position() = $position]/sub1_ru_name)" />
			
			<xsl:choose>
				<xsl:when test="$new_length > 70 or count(//category[cat_id = $category]//sub1_structure[position() = $position + 1]) = 0">
					<xsl:text> </xsl:text><a href="{$catalog_url}{//category[cat_id = $category]/for_path}/">...</a>
				</xsl:when>
				<xsl:otherwise>,<xsl:text> </xsl:text>
					<xsl:call-template name="category-description">
						<xsl:with-param name="category" select="$category"/>
						<xsl:with-param name="position" select="$position + 1"/>
						<xsl:with-param name="length" select="$new_length"/>							
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
	</xsl:template>
	<xsl:template match="sub1_structure" mode="category-description">
		<a href="{$catalog_url}{../../for_path}/{for_path}/"><xsl:value-of select="sub1_ru_name" disable-output-escaping="yes"  /></a>
	</xsl:template>
	
	
	<!-- Цикл для вывода строк ссылок -->
	<xsl:template name="for">
		
		<xsl:param name="limit"/>
		<xsl:param name="page"/>
		<xsl:param name="pre_count_page"/>
		<xsl:param name="post_count_page"/>
		<xsl:param name="i" select="0"/>
		<xsl:param name="items_count"/>
		<xsl:param name="visible_pages"/>
		
		<xsl:variable name="n" select="ceiling($items_count div $limit)"/>
		
		<xsl:variable name="start_page"><xsl:choose>
				<xsl:when test="$page + 1 = $n"><xsl:value-of select="$page - $visible_pages + 1"/></xsl:when>
				<xsl:when test="$page - $pre_count_page &gt; 0"><xsl:value-of select="$page - $pre_count_page"/></xsl:when>
				<xsl:otherwise>0</xsl:otherwise> 
		</xsl:choose></xsl:variable>
		
		<!-- Фильтр и сортировка -->
		<xsl:variable name="filter_order_query">
			<xsl:choose>
				<xsl:when test="count(/mouser/filter/url) and /mouser/filter/url != ''">
					<xsl:value-of select="/mouser/filter/url" /><xsl:if test="count(/mouser/order) and /mouser/order != ''">&amp;order=<xsl:value-of select="/mouser/order"/></xsl:if>
				</xsl:when>
				<xsl:otherwise>
					<xsl:if test="count(/mouser/order) and /mouser/order != ''">?order=<xsl:value-of select="/mouser/order"/></xsl:if>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		
		<!-- Определяем адрес ссылки -->
		<xsl:variable name="number_link">
			<xsl:choose>
				<!-- Если не нулевой уровень -->
				<xsl:when test="$i != 0">page-<xsl:value-of select="$i + 1"/>/</xsl:when>
				<!-- Иначе если нулевой уровень - просто ссылка на страницу со списком элементов -->
				<xsl:otherwise></xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		
		<!-- Выводим ссылку на первую страницу -->
		<xsl:if test="$i = $start_page and $page != 0">
			<li class="first"><a href="{$group_link}{$filter_order_query}">Первая</a></li>
		</xsl:if>
		
		<!-- Ссылка на предыдущую страницу для Ctrl + влево -->
		<xsl:if test="$page != 0 and $i = $start_page">
			<xsl:variable name="prev_number_link">
				<xsl:choose>
					<!-- Если не нулевой уровень -->
					<xsl:when test="($page) > 1">page-<xsl:value-of select="$page"/>/</xsl:when>
					<!-- Иначе если нулевой уровень - просто ссылка на страницу со списком элементов -->
					<xsl:otherwise></xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			
			<li class="prev"><a href="{$group_link}{$prev_number_link}{$filter_order_query}">Предыдущая</a></li>
			<xsl:if test="$page > $pre_count_page"><li rel="pre_{$pre_count_page}" rev="page_{$page}" class="dots">...</li></xsl:if>
		</xsl:if>
			
		<!--
		<xsl:if test="$i = $start_page and $page != 0">
			<a class="p-prev" href="#"></a>
		</xsl:if>
		
		<xsl:if test="$i = ($page + $post_count_page + 1) and $n != ($page+1)">
			<a class="n-next" href="#"></a>
		</xsl:if>
		-->
		
		<xsl:if test="$items_count &gt; $limit and ($page + $post_count_page + 1) &gt; $i">
			
			
			<!-- Ставим ссылку на страницу-->
			<xsl:if test="$i != $page">
				<xsl:if test="($page - $pre_count_page) &lt;= $i and $i &lt; $n">
					<!-- Выводим ссылки на видимые страницы -->
					<li><a href="{$group_link}{$number_link}{$filter_order_query}">
						<xsl:value-of select="$i + 1"/>
					</a></li>
				</xsl:if>
			</xsl:if>
			
			<!-- Не ставим ссылку на страницу-->
			<xsl:if test="$i = $page">
				<li class="active">
					<xsl:value-of select="$i+1"/>
				</li>
			</xsl:if>
			
			<!-- Рекурсивный вызов шаблона. НЕОБХОДИМО ПЕРЕДАВАТЬ ВСЕ НЕОБХОДИМЫЕ ПАРАМЕТРЫ! -->
			<xsl:call-template name="for">
				<xsl:with-param name="i" select="$i + 1"/>
				<xsl:with-param name="limit" select="$limit"/>
				<xsl:with-param name="page" select="$page"/>
				<xsl:with-param name="items_count" select="$items_count"/>
				<xsl:with-param name="pre_count_page" select="$pre_count_page"/>
				<xsl:with-param name="post_count_page" select="$post_count_page"/>
				<xsl:with-param name="visible_pages" select="$visible_pages"/>
			</xsl:call-template>
		</xsl:if>
		 
		
		<!-- Ссылка на следующую страницу для Ctrl + вправо -->
		<xsl:if test="($n - 1) > $page and $i = $page">
			<xsl:if test=" $n > ($page + $post_count_page + 1) "><li rel="n_{$n}" rev="page_{$page}"  class="dots">...</li></xsl:if>
			<li class="next"><a href="{$group_link}page-{$page+2}/{$filter_order_query}">Следующая</a></li>
			<!-- Выводим ссылку на последнюю страницу -->
			<li class="last"><a href="{$group_link}page-{$n}/{$filter_order_query}" >Последняя</a></li>
		</xsl:if>
		
		
	</xsl:template>
	

</xsl:stylesheet> 